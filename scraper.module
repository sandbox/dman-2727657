<?php
/**
 * @file
 *   Scraper functions and hook implimentations.
 */


/**
 * Mostly just error-checking and diagnostics around drupal_http_request().
 *
 * @param $url
 * @return string
 */
function scraper_get_content($url) {
  $strings = array(
    '%url' => $url,
    '!source' => l(basename($url), $url),
  );

  // Ensure it's a reasonable URL.
  if (!valid_url($url)) {
    watchdog(__FUNCTION__, "Provided URL %url looks invalid. Not attempting to scrape it.", $strings, WATCHDOG_WARNING);
    return "";
  }

  watchdog(__FUNCTION__, "Fetching content from remote URL %url to scrape it.", $strings, WATCHDOG_INFO);
  // We need to use reasonable timeouts, respect proxies and redirects...
  $http_options = array(
    'timeout' => 10,
  );
  $response = drupal_http_request($url, $http_options);

  if (!empty($response->error)) {
    $strings['%code'] = $response->code;
    $strings['%error'] = $response->error;
    // This is a pain, but non-fatal.
    watchdog(__FUNCTION__, "URL fetch from remote %url failed, aborting. Maybe it's not available from the source. %code : %error", $strings, WATCHDOG_ERROR);
    return "";
  }
  watchdog(__FUNCTION__, 'URL fetch from remote !source succeeded.', $strings, WATCHDOG_INFO);
  return $response->data;
}


/**
 * Perform an Xpath extraction.
 *
 * Mostly just error-checking and diagnostics around DOMXpath::query().
 *
 * This may return an empty string, but that may be due to
 * the result being literally that,
 * the page not containing the desired element (which is legal)
 * Or possibly other errors (not legal, but the processes should continue on.
 *
 * @param string $content
 * @param string $xpath
 * @param boolean $unwrap
 * @return string
 */
function scraper_extract_with_xpath($content, $xpath, $unwrap = FALSE) {
  $elements = NULL;
  $result = '';
  $strings['%xpath'] = $xpath;

  // XML fails often. Often tragically.
  $doc = new DOMDocument();
  try {
    $doc->loadHTML($content);
    $xpath_lookup = new DOMXpath($doc);
    $elements = $xpath_lookup->query($xpath);
  }
  catch (Exception $e) {
    $strings['%message'] = $e->getMessage();
    watchdog(__FUNCTION__, "XML unhappiness when running xpath %xpath over content : %message", $strings, WATCHDOG_ERROR);
  }

  if (is_null($elements)) {
    watchdog(__FUNCTION__, 'XPath scrape of the content failed.', $strings, WATCHDOG_INFO);
    return $result;
  }

  // We expect to get one element, but if we find several, concatenate them.
  foreach ($elements as $found) {
    // We either return the item itself, or just its contents.
    if ($unwrap) {
      foreach ($found->childNodes as $child_node) {
        $result .= $doc->saveXML($child_node);
      }
    }
    else {
      $result .= $doc->saveXML($found);
    }
  }
  return $result;
}


/**
 * Allow us to tell if a feed item is being imported or 'skipped'.
 *
 * Feeds does not expose this value, so we do it here.
 * This flag will now be available as a condition to be cheked when building
 * a rule.
 *
 * This utility is seemingly unrelated to scraping, but it's neccessary
 * when using Feeds (with Feeds+Rules to grab full remote content) to
 * detect if the feed item is actually being imported or not.
 * Neccessary to avoid remotely data-scraping every item all the time.
 *
 * The trigger Feeds calls "Before saving an item imported ..." actually runs on
 * ALL items being evaluated, so we need to filter out the duds.
 *
 * Implements hook_entity_property_info_alter().
 *
 * @inheritdoc
 */
function scraper_entity_property_info_alter(&$info) {
  foreach ($info as $entity_type => $entity_info) {
    // Should check if entity is a feed?
    $info[$entity_type]['properties']['feeds_item_skip'] = array(
      'label' => 'Feeds item skip',
      'type' => 'boolean',
      'description' => t('Whether the feed item is set to be skipped.'),
      'getter callback' => 'scraper_get_feeds_item_skip_entity_callback',
    );
    // Would be nice to have the feed items URL directly available too.
    // However that is not always present in the $node->feeds_item unless
    // the admin has mapped it.
    $info[$entity_type]['properties']['feeds_item_url'] = array(
      'label' => 'Feeds item URL',
      'type' => 'text',
      'description' => t('URL associated with the individual feed item (must be mapped in Feeds Mappings).'),
      'getter callback' => 'scraper_get_feeds_item_url_entity_callback',
    );
  }
}


/**
 * Gets the feed items 'skip' state for an entity for use in entity metadata.
 *
 * Most commonly, 'skip' indicates a feed item being scanned that already has
 * been imported.
 *
 * This value is probably only present during an actual import.
 *
 * @inheritdoc
 */
function scraper_get_feeds_item_skip_entity_callback($entity, array $options, $name, $entity_type) {
  if (isset($entity->feeds_item)) {
    // 'skip' is not even set if the item is to be processed.
    return !empty($entity->feeds_item->skip);
  }
  // Invalid question, but gotta return something.
  return FALSE;
}

/**
 * Gets the feed items 'url' value for an entity for use in entity metadata.
 *
 * This must have been deliberately set in the Feed mapping,
 * or the data is not available.
 *
 * @inheritdoc
 */
function scraper_get_feeds_item_url_entity_callback($entity, array $options, $name, $entity_type) {
  if (isset($entity->feeds_item)) {
    // It's always defined, though not always populated.
    if (!empty($entity->feeds_item->url)) {
      return $entity->feeds_item->url;
    }
    else {
      // Failure here is pretty easy to overlook, but hard to trace, so it's
      // worth a watchdog log.
      watchdog(__FUNCTION__, 'Expected feed item URL was not set. You probably need to ensure that URL is mapped to URL in the Feeds mapping configuration for this data to become available', array(), WATCHDOG_NOTICE);
    }
  }
  return '';
}

/**
 * Advertise that we provide a file_fetcher as a feeds tamper action.
 *
 * Implements hook_ctools_plugin_api().
 * @inheritdoc
 */
function scraper_ctools_plugin_api($owner, $api) {
  if ($owner == 'feeds_tamper' && $api == 'plugins') {
    return array('version' => 2);
  }
  return NULL;
}

/**
 * Advertise where we keep our feeds tamper plugins.
 *
 * Implements hook_ctools_plugin_directory().
 * @inheritdoc
 */
function scraper_ctools_plugin_directory($module, $plugin) {
  if ($module == 'feeds_tamper') {
    return 'plugins/feeds_tamper';
  }
  return NULL;
}

