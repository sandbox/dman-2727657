# Scraper

Plugin routines for fetching and parsing remote content.

Rather than being an all-in-one utility, this is mostly
a set of plugins that can be strung together inside the Drupal
ecosystem of modules, actions data sources and processes.

This module has *soft* dependencies on Feeds and Rules.

## Feeds Tamper
When used with **Feeds Tamper**, A new tamper action becomes
available, to take an URL (like the feeds source itself)
anf fetch, parse and scrape the contents - useful to grap additional info that
 the raw feed did not provide.

## Rules Action
When used with **Rules** A new Rules action becomes available.
This Rule also takes an URL and some transformation parameters,
Fetches and processes a result, and makes the resulting data available for
use in a further action, such as updating a field.
