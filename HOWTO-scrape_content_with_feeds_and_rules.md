
This is similar to the process for scraping with feeds tamper,
 but it's more efficient as you can defer the scrape until after Feeds has
 decided that an item genuinely needs to be saved.

What we do is set up a feed imprt to fetch just the short description text
 as normal, but then use the Rules event trigger
 "Before saving an item imported via XXX."
  and run a Rules Action "Fetch data from an URL"
  to set the body before saving.


## If you have a teaser-only feed, but you need the full text:

* Enable Feeds, Rules, and this module.
* Set up the feed with GUID, tag, type and date and mappings as usual.
* At this point, be sure to also map the URL to the URL in the feeds mappings,
  so that the feeds_item URL will be available to the rule later!
* It's good if your target content type contains an additional field for
  "Original URL", and you map the feed item URL into that.
  You can choose to not display it, but it's useful to hang on to that info.

Test that this is running as usual before proceeding.

* Create a new Rule, eg "Fetch content from feeds source"
* Add the Event, which is provided by Feeds, and will be of the form
  "Before saving an item imported via XXX."
* To ensure this does not run ALL the time, but only when an item is really
  being saved or updated, add a
  ``Condition : Data comparison : node:feed-skip = FALSE.``
* Now you can add the
  ``Action: Fetch data from an URL``
* For this action, set the data selector to ``node:feed-item-url``
* Once the source data is fetched, it will now be available to Rules,
  so you add another action which will be:
  ``Action:  Set a data value : node:body:value : result``


