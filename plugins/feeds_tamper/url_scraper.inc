<?php
/**
 * @file
 * Feeds tamper plugin to fetch content from remote URLS.
 *
 * If the source data is a remote URL,
 * fetch it,
 * scrape it,
 * and use the result as the field value.
 */

$plugin = array(
  'form'     => 'scraper_tamper_form',
  'callback' => 'scraper_tamper_callback',
  'name'     => 'Scrape URL',
  'multi'    => 'skip',
  'category' => 'Other',
);

/**
 * Settings form for the tamper settings.
 */
function scraper_tamper_form($importer, $element_key, $settings) {
  $form = array();

  $form['help'] = array(
    '#type' => 'markup',
    '#markup' => t('This plugin expects to be given a full remote URL as a page to fetch. The value of the field being tampered will be populated with data retrieved from there.'),
  );
  $form['xpath'] = array(
    '#type' => 'textfield',
    '#title' => t('XPath pattern'),
    '#default_value' => isset($settings['xpath']) ? $settings['xpath'] : '',
    '#description' => t('Pattern to look for inside the fetchd document.'),
  );
  return $form;
}

/**
 * Fetcher callback.
 */
function scraper_tamper_callback($result, $item_key, $element_key, &$field, $settings, $source) {
  // Seems I can't use Feeds log(). Make noise in watchdog instead.
  $url = $field;
  $strings = array(
    '%url' => $field,
    '!source' => l(basename($field), $field),
  );
  $default_settings = array(
    'unwrap' => FALSE,
  );
  $settings += $default_settings;

  $content = scraper_get_content($url);

  if (empty($content)) {
    watchdog(__FUNCTION__, 'URL fetch from remote !source failed to return a result. (Rule: %rule_label)', $strings, WATCHDOG_INFO);
    return array('result' => $content);
  }
  watchdog(__FUNCTION__, 'URL fetch from remote !source succeeded.', $strings, WATCHDOG_INFO);

  if (empty($settings['xpath'])) {
    // No further processing is wanted. Return raw result.
    return array('result' => $content);
  }

  // Now scrape.
  $content = scraper_extract_with_xpath($content, $settings['xpath'], $settings['unwrap']);
  if (is_string($content)) {
    // So reason why it wouldn't be a string, but let's be careful.
    $field = $content;
  }
}
