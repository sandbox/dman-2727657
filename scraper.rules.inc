<?php

/**
 * Provides Rules Actions.
 *
 * Implements hook_rules_action_info().
 */
function scraper_rules_action_info() {
  $actions = array(
    'scraper_fetch_data' => array(
      'label' => t('Fetch data from an URL'),
      'parameter' => array(
        'url' => array(
          'type' => array('text'),
          'label' => t('Source URL'),
          'description' => t('URL to fetch.'),
        ),
        'xpath' => array(
          'type' => 'text',
          'restriction' => 'input',
          'label' => t('XPath pattern'),
          'description' => t('Pattern to look for inside the fetched document. EG <code>//div[@class="content"]/child::*</code>'),
        ),
        'unwrap' => array(
          'type' => 'boolean',
          'restriction' => 'input',
          'label' => t('Unwrap'),
          'description' => t('Many xpath patterns return a result that includes a named wrapper element. Choose to unwrap if you just want the <em>content</em> (InnerHTML) of the found element, discarding the wrapper.'),
        ),
      ),
      'group' => t('Data'),
      'provides' => array(
        'fetched_content' => array(
          'type' => 'text',
          'label' => t('Fetched content'),
        ),
      ),
    ),
  );

  return $actions;
}

/**
 * Callback for the fetch_data action.
 *
 * Fetcher data as part of a rule.
 *
 * On serious fails, return the URL as provided.
 * On a soft fail (no error, but element not found) return an empty string.
 *
 * @param string $url
 * @param string $xpath
 * @param bool $unwrap
 * @param array $settings
 *   Parameters for the current action, from the settings form.
 * @param \RulesState $state
 * @param \RulesAction $element
 *
 * @return string
 */
function scraper_fetch_data($url, $xpath, $unwrap, $settings, $state, $element) {
  $strings = array(
    '%url' => $url,
    '!source' => l(basename($url), $url),
  );

  // Fetch.
  $content = scraper_get_content($url);

  if (empty($content)) {
    watchdog(__FUNCTION__, 'URL fetch from remote !source failed to return a result. (Rule: %rule_label)', $strings, WATCHDOG_INFO);
    return array('fetched_content' => $content);
  }
  watchdog(__FUNCTION__, 'URL fetch from remote !source succeeded. (Rule: %rule_label)', $strings, WATCHDOG_INFO);

  if (empty($xpath)) {
    // No further processing is wanted. Return raw result.
    return array('fetched_content' => $content);
  }

  // Now scrape.
  $content = scraper_extract_with_xpath($content, $xpath, $unwrap);

  return array('fetched_content' => $content);
}
